# README #

# Basic Description #

uGrounds is a simple solution for most Newgrounds API features.

In its current state uGrounds is unfinished, but functional for much of the Newgrounds API. Several of the available commands aren’t yet implemented at this time. Also, uGrounds currently only works directly on the Newgrounds portal; not for Unity games in other portals or standalone (though in theory both could work if uGrounds and Newgrounds Passport could be made to play nicely).

# Usage #

The main file **Newgrounds.JS** needs to be attached to a single object in the first scene of your Unity game. This can be your main character or a standalone object set just to handle communications with Newgrounds API. It will not be destroyed when you change scenes; in order to keep from needing to download the same data multiple times, so take this into account when picking the object to attach it to.

## Member Variables ##

The first five member variables of Newgrounds.JS are to be set up on installation. The first three require values.

* **APIID:** The API id of your game here. 
* **Password:** The Encryption Key of your game here.
* **Preload Settings:** Should your badges and scoreboards be loaded when the game starts?
* **Version Number:** Current version number of your game (used by the API to check if up-to-date)
* **Default Username:** If the player isn’t logged into Newgrounds use this as their name.

The remaining member variables are used locally and only exposed in editor for both debugging purposes and for script accessibility (if you wanted to reference the player's Newgrounds account name, for example).

* **Session ID:** Used by the API to validate that the player is currently playing. [You shouldn’t ever access this directly.]
* **User ID:** Current player’s profile number on Newgrounds.
* **Username:** Current player’s Newground’s screenname (will show your Default Username if the player isn’t logged in).
* **Publisher ID:** (1) if Newgrounds. Other numbers are different flash portals.
* **Medals:** An array of the medals of your game; members of a unique class with the following attributes:
	* *Id:* Newgrounds Medal ID number. [You shouldn’t ever access this directly.]
	* *Name:* The name of your medal as you typed into the Newgrounds API page.
	* *Points:* Integer value of points awarded the player.
	* *Difficulty:* 1=easy, 2= moderate, 3=challenging, 4=difficult, 5=brutal 
	* *Unlocked:* Has the player gotten this medal before?
	* *Secret:* Is this a secret medal?
	* *Description:* The description of your medal as you typed into the Newgrounds API page.
	* *Url:* The URL where your medal Icon is hosted.
	* *Icon:* A Texture2D object of the medal Icon (if it has been downloaded).
* **SaveGroups:** An array of the SaveGroups of your game; members of a class with these attributes:
	* *Id:* Newgrounds SaveGroup ID number. [You shouldn’t ever access this directly.]
	* *Name:* The name of your SaveGroup as you typed into the Newgrounds API page.
	* *Type:* System, Public, Private or Moderated.
	* *Keys:* An array of the keys of your savegroup; members of a class with these attributes:
		* **Id:** Newgrounds Key ID number. [You shouldn’t ever access this directly.]
		* **Name:** The name of your key as you typed into the Newgrounds API page.
		* **Type:** Float, Int, String or Boolean
	* *Ratings:* An array of the ratings of your savegroup; members of a class with these attributes:
		* **Id:** Newgrounds Rating ID number. [You shouldn’t ever access this directly.]
		* **Name:** The name of your rating as you typed into the Newgrounds API page.
		* **Min:** Minimum value of this rating.
		* **Max:** Maximum value of this rating.
.		* **isFloat:** True if it’s a float; false if it’s an int.
* **Board Names:** An array of the names of your Scoreboards.
* **Board IDs:** An array of the ID numbers of your Scoreboards. [You shouldn’t ever access this directly.]

## Functions ##

**Note:** All finished general use functions are written to take a single argument to facilitate use of the global "SendMessage()" for communication with the uGrounds object.

### General Use ###

* **getMedals()** - Retrieves the array of your medals from the Newgrounds server and updates it locally. [Unnecessary if preloadSettings is true.]
* **loadSettings ()** Retrieves the array of your medals, score boards and save groups from the Newgrounds server and updates it locally. [Unnecessary if preloadSettings is true.]
* **downloadMedal (MedalName: String)** - Downloads the icon of the named medal and saves it to the medal.icon attribute.
* **unlockMedal (MedalName: String)** - Unlocks medal with MedalName. Throws an exception if MedalName doesn’t exist (or you misspell it).
* **findMedal (MedalName: String)** - Searches Medals[] for a medal with MedalName. Throws an exception if MedalName doesn’t exist (or you misspell it). Returns the local index of the medal (position in the array) if successful.
* **postScore (score : int, BoardName : String)** - Posts the score to a scoreboard with BoardName. Throws an exception if the board doesn’t exist.
* **loadMySite ()** - Logs a referral stat with Newgrounds. (Doesn't actually load the site.)
* **saveFile (saveGroup: String, fileName: String, description: String, file: String, thumbnail: Texture2D)** - Saves file (can be any file, but must be stored as a string) with fileName and thumbnail to saveGroup. Doesn’t support tags, yet. (Functionality incomplete; files can be saved, but not loaded).

### Background/Utilities ###

* **parseJSON (JSON : String)** - Used to read the Developer Gateway’s responses into memory.
* **registerSession ()** - Called automatically 5 seconds after the game starts. If Preload Settings is true; retrieves the array of your medals, score boards and save groups from the Newgrounds server and updates it locally. Regardless; it lets Newgrounds know that someone is playing your game. [You should never call this command manually.]
* **securePacket (seed: String, text: String)** - Encrypts and sends the server a secure packet with seed as the MD5Hash Seed and text as the main message to send.
* **encrypt (seed : String, text : String)** - Encrypts a secure packet with seed as the MD5Hash Seed and text as the main message.
* **decrypt (g : String)** - Decrypts a packet to allow reading. (for debug purposes)
* **genSeed ()** - Creates a random 8-16 alphanumeric character string.
* **Md5Sum (strToEncrypt: String)** - Part of the encryption process.
* **EnDeCrypt(text : String)** - Part of the encryption and decryption processes.
* **StrToHexStr(str : String)** - Part of the encryption process.
* **HexStrToStr(hexStr : String)** - Part of the decryption process.
* **RC4Initialize()** - Part of the encryption process.
* **radix(strToEncrypt: String)** - Part of the encryption process.
* **deradix (strToEncrypt: String)** - Part of the decryption process.
* **receiveURL (data: String)** - Parses the embed URL to get session data from Newgrounds.

## Contribution guidelines ##

Anyone is welcome to fork to help create more features; both programmers and artists welcome. The goal of the project is to be as close of a facsimile of the flash functionality as possible in Unity. [Thus an artist to design similar 'badge unlocked' popups or a 3D copy of the Newgrounds tank for opening credits is welcome.]

### Current To-Do ###


1. **Task scheduling/Error Handling** - Code scheduler to attempt to contact the server five times if it fails (and internet connection is valid). It would also correct conflicts caused when two packets are sent too closely together.
1. **Referrals** - opening Newgrounds and other pages via button press. I believe Unity will need to have the address compiled with it, instead of being able to simply use the server, which is fine.
1. **High Score Query** - Downloading portions of the high score tables to game data.
1. **Default internal High Score Screen** - (Art and UI design).
1. **Medal Popups** - (Art and UI Design).
1. **Default Medal Screen** - (Art and UI Design).
1. **Local Medal Backup** - Using PlayerPrefs hashtable to allow the player to unlock a medals and high scores offline and report it to newgrounds later. Information should be saved in encrypted format to prevent manipulation.
1. **Save File Management** - Save Game; Load Game; List Save Files; Rate Save Files commands.
1. **Save Local** -  Function to use Unity's PlayerPrefs hashtable to store larger files, by encoding to a string and converting back.
1. **Default Save File Screens** - Two, one for normal saves; one for level editors. (Art and UI Design).
1. **Click SaveFile loading** - Access the buttons outside the game that send commands to the iFrame to load files. [Unsure if possible with Unity.]
1. **Save File Lookup** - Using queries to search all saveFiles for level editors and the like. (Probably require packetsniffing to see the flash version does this, due to lack of documentation.)

# Contact #

bcadren.games@gmail.com

bcadren.newgrounds.com